as yet unclassified
formsFromFileNamed: aFile
	^ (self on: aFile asFileReference readStream binary)
			readHeader;
			readBody;
			yourself