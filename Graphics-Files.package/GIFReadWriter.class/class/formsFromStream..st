as yet unclassified
formsFromStream: aBinaryStream
	^ (self on: aBinaryStream)
		readHeader;
		readBody;
		yourself