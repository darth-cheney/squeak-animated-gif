as yet unclassified
delays
	"Respond with an ordered collection of Frame delay values"
	^ frames collect: [ :frame | frame delay ]