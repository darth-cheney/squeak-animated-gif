accessing
form
	"By default, answer with the first Form available in the
	ImageFrames collection. If there are not any frames, answer nil"
	frames ifNil: [ ^ nil ].
	frames ifEmpty: [ ^ nil ].
	^ frames first form.