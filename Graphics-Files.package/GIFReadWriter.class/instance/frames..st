accessing
frames: aCollectionOfImageFrames
	"Set the receiver's underlying collection of
	ImageFrame objects. Used when attempting to write
	out GIF images"
	frames := aCollectionOfImageFrames