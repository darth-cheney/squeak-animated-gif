accessing
nextImage
	"This method ensures older compatibility with ImageReadWriter.
	We respond with the Form corresponding to the *first image* on
	the receiver's read byte stream"
	self
		readHeader;
		readBody.
	^ self form.
	