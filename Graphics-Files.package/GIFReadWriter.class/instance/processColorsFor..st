private - decoding
processColorsFor: anImageFrame
	"Colors can only be mapped after the GCE has been evaluated
	for a given image frame. We perform this action using either
	the local or global color table for this frame's form"
	| colorTable |
	colorTable := localColorTable ifNil: [ colorPalette ].
	
	"Use a copy so we don't mess up the global color table as we parse"
	colorTable := colorTable copyFrom: 1 to: colorTable size.
	
	transparentIndex 
		ifNotNil: [ 
			transparentIndex + 1 > colorTable size
				ifTrue: [ 
					colorTable := colorTable
										forceTo: transparentIndex + 1
										paddingWith: Color white ].
				colorTable
					at: transparentIndex + 1
					put: Color transparent ].
	anImageFrame form colors: colorTable.