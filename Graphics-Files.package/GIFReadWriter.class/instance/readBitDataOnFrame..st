private - decoding
readBitDataOnFrame: aFrame
	"using modified Lempel-Ziv Welch algorithm."
	| initCodeSize  packedBits hasLocalColor localColorSize maxOutCodes decoder c  bytes |
	maxOutCodes := 4096.
	offset := self readWord @ self readWord.	"Image Left@Image Top"
	width := self readWord.
	height := self readWord.
	"---
	Local Color Table Flag        1 Bit
	Interlace Flag                1 Bit
	Sort Flag                     1 Bit
	Reserved                      2 Bits
	Size of Local Color Table     3 Bits
	----"
	packedBits := self next.
	interlace := (packedBits bitAnd: 64) ~= 0.
	hasLocalColor := (packedBits bitAnd: 128) ~= 0.
	localColorSize := 1 bitShift: (packedBits bitAnd: 7) + 1.
	hasLocalColor 
		ifTrue: [ 
			localColorTable := self readColorTable: localColorSize ]
		ifFalse: [ localColorTable := nil ].
	pass := 0.
	xpos := 0.
	ypos := 0.
	rowByteSize := (width + 3) // 4 * 4.
	bytes := ByteArray new: rowByteSize * height.

	initCodeSize := self next.

	c := ColorForm 
		extent: width@height
		depth: 8. 

	decoder := LzwGifDecoder new.
	decoder 
		codeStream: stream;
		minimumCodeSize: initCodeSize;
		maxCode: maxOutCodes;
		onDecodedBit: [ :bit |
			bytes
				at: (ypos * rowByteSize + xpos + 1)
				put: bit.
			self updatePixelPosition ].
	decoder decode.
	c bits copyFromByteArray: bytes.
	^ c