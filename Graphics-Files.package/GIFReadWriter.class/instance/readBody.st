private - decoding
readBody
	"Read the GIF blocks. Modified to return a frame."
	| block frame |
	frame := nil.
	frames := OrderedCollection new.
	[ stream atEnd ] whileFalse: [ 
		block := self next.
		
		"If we have reached the terminator byte, return."
		block = Terminator ifTrue: [ ^ frame ].
		block = ImageSeparator 
			ifTrue: [ 
				frame ifNil: [ frame := AnimatedImageFrame new ].
				frame form: (self readBitDataOnFrame: frame). "Adjusting message for testing"
				frame offset: offset. "Set from instance var, which is set in readBitData"
				frame form offset: offset. "Set the offset on the underlying Form as well"
				
				frames add: frame.
				self processColorsFor: frame.
				self next = Terminator ifTrue: [ ^ frames last ].
				frame := nil. ]
			ifFalse: 
				[ "If it's not actual image data, perhaps
					it's an Extension of some kind (there can be several)"
					block = Extension 
						ifTrue: [ 
							frame ifNil: [ frame := AnimatedImageFrame new ].
							self readExtensionBlock: block withFrame: frame ]
						ifFalse: [ ^ self error: 'Unknown Bytes!' ] ] 
		].
	^ frames.