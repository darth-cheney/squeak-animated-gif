private - decoding
readCommentExtension
	| blockTerminator |
	blockTerminator := self next.
	blockTerminator > 0
		ifTrue: [ comment := self next: blockTerminator.
			blockTerminator := self next ].
	blockTerminator = 0
		ifFalse: [ ^ self error: 'Invalid Block Terminator' ]