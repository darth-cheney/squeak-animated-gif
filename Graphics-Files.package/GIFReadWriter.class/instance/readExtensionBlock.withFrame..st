private - decoding
readExtensionBlock: aGifBlock withFrame: anImageFrame
	"Determine which type of extension block we are
	looking at. The most common is the Graphic Control Extension (GCE)
	which tells us information about the image frame, including delays
	offsets in the canvas, and how to dispose of the frame in animation"
	| extensionType packedByte delayByte1 delayByte2 |
	extensionType := self next.
	
	"255 is an Application Extension.
	 This seems to always be the NETSCAPE
	 extension, which has looping information.
	This extension does not affect individual frames,
	but rather sets the loopCount for the whole image"
	extensionType = 255 ifTrue: [ 
		^ self readApplicationExtension ].
	
	
	"249 Corresponds to the GCE"
	extensionType = 249 ifTrue: [ 
		self next = 4 ifFalse: [ ^ self "The GIF is likely corrupt in this case" ].
		"====
		Reserved                      3 Bits (Ignore)
		Disposal Method               3 Bits 
		User Input Flag               1 Bit  (Ignore)
		Transparent Color Flag        1 Bit  (Need to Implement)
		==="
		packedByte := self next.
		delayByte1 := self next.
		delayByte2 := self next.
		transparentIndex := self next.
		(packedByte bitAnd: 1) = 0 "Changed to see if other endian is the real end..."
			ifTrue: [ transparentIndex := nil ].
		anImageFrame 
			disposal: (self readDisposal: packedByte);
			"Delay time is stored as 2 bytes unsigned"
			delay: (delayByte2 * 256 + delayByte1) * 10.
		self next = 0 ifFalse: [ ^ self error: 'Corrupt GCE Block!' ].
		^ self ].

	extensionType = 254 ifTrue: [ 
		^ self readCommentExtension ].

	"If you get to this point, we don't know the Extension Type"
	^ self error: 'Unknown GIF Extension: ',(extensionType asString).