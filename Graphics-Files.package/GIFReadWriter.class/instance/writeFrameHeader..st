accessing
writeFrameHeader: anImageFrame
	"Write any Extensions and/or headers that apply
	to individual frames/subimages"
	| interlaceByte |
	anImageFrame delay notNil | transparentIndex notNil ifTrue: [ 
		self writeGCEForFrame: anImageFrame ].
	
	"Next is the image descriptor"
	self 
		nextPut: ImageSeparator;
		writeWord: (anImageFrame offset x);
		writeWord: (anImageFrame offset y);
		writeWord: (anImageFrame form extent x);
		writeWord: (anImageFrame form extent y).
	
	interlaceByte := interlace
		ifTrue: [ 64 ]
		ifFalse: [ 0 ].
	self nextPut: interlaceByte
	