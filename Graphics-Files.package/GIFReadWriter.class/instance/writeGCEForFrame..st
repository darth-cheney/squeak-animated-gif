private - encoding
writeGCEForFrame: anAnimatedImageFrame
	"Writes a Graphics Control Extension onto
	the output stream for the given image frame"
	| nextDelay packedByte |
	nextDelay := anAnimatedImageFrame delay.
	anAnimatedImageFrame delay ifNil: [ nextDelay := 0 ].
	"Set the bits of the packed byte"
	"====
		Reserved                      3 Bits (Ignore)
		Disposal Method               3 Bits 
		User Input Flag               1 Bit  (Ignore)
		Transparent Color Flag        1 Bit 
		==="
	packedByte := 0.
	transparentIndex
		ifNotNil: [ packedByte := 1 ].
	packedByte := self 
		writeDisposal: (anAnimatedImageFrame disposal)
		toPackedByte: packedByte.
	
	self 
		nextPut: Extension;
		nextPutAll: #(249 4) asByteArray;
		nextPut: packedByte;
		"nextPut: (transparentIndex
				ifNil: [ 0 ]
				ifNotNil: [ 9 ]);"
		writeWord: nextDelay // 10;
		nextPut: (transparentIndex ifNil: [ 0 ]);
		nextPut: 0.