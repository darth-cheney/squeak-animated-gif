private - encoding
writeHeader
	| byte |
	"Write the overall image file header onto the
	output stream. This includes the global information
	about the file, such as canvasWidth etc. Only do so
	if the stream is in the initial position."
	stream position = 0 ifFalse: [ ^ self ].

	self nextPutAll: 'GIF89a' asByteArray.
	self writeWord: width.	"Screen Width"
	self writeWord: height.	"Screen Height"
	byte := 128.	"has color map"
	byte := byte bitOr: (bitsPerPixel - 1 bitShift: 5).	"color resolution"
	byte := byte bitOr: bitsPerPixel - 1.	"bits per pixel"
	self nextPut: byte.
	self nextPut: 0.	"background color."
	self nextPut: 0.	"reserved / unused 'pixel aspect ratio"
	colorPalette do: 
		[ :pixelValue | 
		self
			nextPut: ((pixelValue bitShift: -16) bitAnd: 255);
			nextPut: ((pixelValue bitShift: -8) bitAnd: 255);
			nextPut: (pixelValue bitAnd: 255) ].
	loopCount notNil ifTrue: 
		[ self writeNetscapeExtension ].