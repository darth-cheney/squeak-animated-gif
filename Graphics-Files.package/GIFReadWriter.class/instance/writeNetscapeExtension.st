private - encoding
writeNetscapeExtension
	"Writes a GIF Application Extension corresponding
	to the NETSCAPE2.0 version, with specifies the loopCount."
	self
		nextPut: Extension;
		nextPut: 255; "Indicates Application Extension"
		nextPut: 11; "Indicates how many bytes follow, almost always 11"
		nextPutAll: ('NETSCAPE2.0' asByteArray);
		nextPut: 3;
		nextPut: 1;
		writeWord: (loopCount ifNil: [ 0 ]);
		nextPut: 0.