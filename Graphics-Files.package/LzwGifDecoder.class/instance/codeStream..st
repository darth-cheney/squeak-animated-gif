private
codeStream: aReadableStream 
	"Set the stream of encoded bytes we will decode
	to be the internal codeStream. We read off the first
	byte immediately, which tells us how many subsequent bytes
	to use in our buffer for decoding"
	| chunkSize |
	codeStream := aReadableStream.
	chunkSize := codeStream next.
	codeStreamBuffer := ReadStream on: (codeStream next: chunkSize).