private
handleCode: anInteger withPreviousCode: prevInteger on: aWriteStream
	"Check for the code in the tables
	and perform the appropriate LZW action"
	| first searchIndex searchStack |
	"The code already exists in the table"
	anInteger < nextAvailableCode
		ifTrue: [ 
			anInteger < clearCode
				"If it's less than the clearCode
				then it is one of the original entries
				and we can simply use the suffix"
				ifTrue: [ 
					first := (suffixTable at: (anInteger + 1)).
					self writeBit: first on: aWriteStream ]
				"Otherwise we need to loop backwards along
				the prefix index values and take the suffix each
				time"
				ifFalse: [ 
					searchStack := OrderedCollection new.
					searchIndex := anInteger + 1.
					[ searchIndex > clearCode ] whileTrue: [ 
						searchStack add: (suffixTable at: searchIndex).
						searchIndex := (prefixTable at: searchIndex) + 1 ].
					searchStack add: (suffixTable at: searchIndex).
					first := searchStack last.
					searchStack reverseDo: [ :int |
						self writeBit: int on: aWriteStream ] ]. 
			]
		ifFalse: [ 
			"Here, the incoming code is not yet in the code tables"
			prevInteger < clearCode
				ifTrue: [ 
					first := (suffixTable at: (prevInteger + 1)).
					self
						writeBit: first on: aWriteStream;
						writeBit: first on: aWriteStream.
					 ]
				ifFalse: [ 
					searchStack := OrderedCollection new.
					searchIndex := prevInteger + 1.
					[ searchIndex > clearCode ] whileTrue: [ 
						searchStack add: (suffixTable at: searchIndex).
						searchIndex := (prefixTable at: searchIndex) + 1 ].
					searchStack add: (suffixTable at: searchIndex).
					first := searchStack last.
					searchStack reverseDo: [ :int |
						self writeBit: int on: aWriteStream ].
					self writeBit: first on: aWriteStream ]. 
			].
		"We add prevCode and the new
		suffix to a new entry in the code table, but
		only if we aren't at the max. NOTE: due to
		GIF 89a spec's 'deferred clear', if you get to
		the maxCode and haven't seen a clear, you stop
		writing to the tables but continue querying."
		nextAvailableCode >= maxCode
			ifFalse: [ 
				suffixTable at: (nextAvailableCode + 1) put: first.
				prefixTable at: (nextAvailableCode + 1) put: prevInteger ].
		self checkCodeSize.