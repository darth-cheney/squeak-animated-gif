private - packing
nextByte
	| nextChunkSize |
	codeStreamBuffer atEnd
		ifTrue: [ 
			nextChunkSize := codeStream next.
			nextChunkSize = 0 ifTrue: [ ^ self error: 'Next chunk size was 0!' ].
			codeStreamBuffer := (codeStream next: nextChunkSize) readStream ].
	^ codeStreamBuffer next.