writing
writeBit: anInteger on: aWriteStream
	"Write the incoming decoded value onto a
	writestream. If I have an outBlock set, 
	send this value also"
	aWriteStream nextPut: anInteger.
	outBlock ifNil: [ ^ self ].
	outBlock value: anInteger.