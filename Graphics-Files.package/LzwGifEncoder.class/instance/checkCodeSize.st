private
checkCodeSize
	"Determine whether or not we need to increment
	the codeSize"
	(nextAvailableCode > maxCode and: [ codeSize < 12 ])
		ifTrue: [ 
			codeSize := codeSize + 1.
			maxCode := (1 bitShift: codeSize) - 1 ].