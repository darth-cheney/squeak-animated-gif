accessing
dimensions: anExtentPoint
	"Set the extent (as point) of the
	image that will be encoded"
	dimensions := anExtentPoint