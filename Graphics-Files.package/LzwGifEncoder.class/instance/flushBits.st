private - bits access
flushBits
	numLeftoverBits = 0 ifFalse: 
		[ self nextBytePut: bitBuffer.
		numLeftoverBits := 0 ].
	self flushBuffer