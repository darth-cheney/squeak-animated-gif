as yet unclassified
fromGIFReader: aGIFReader
	self
		stepTime: aGIFReader delays first;
		images: (self composedFormsFromReader: aGIFReader);
		yourself