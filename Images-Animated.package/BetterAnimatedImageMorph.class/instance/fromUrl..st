as yet unclassified
fromUrl: url
	| resp reader |
	resp := WebClient httpDo: [ :client | client httpGet: url ].
	(resp code >= 400)
		ifTrue: [ Error signal: 'Bad Response: ', resp status ].
	((resp headerAt: 'Content-Type') = 'image/gif')
		ifFalse: [ Error signal: 'Response is not a GIF type: ', (resp headerAt: 'Content-Type')].
	reader := GIFReadWriter on: resp content asByteArray readStream.
	reader
		readHeader;
		readBody.
	self fromGIFReader: reader