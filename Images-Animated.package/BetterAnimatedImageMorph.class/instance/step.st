as yet unclassified
step
	((imageIndex + 1) > images size)
		ifTrue: [ imageIndex := 1 ]
		ifFalse: [ imageIndex := imageIndex + 1].
	self image: (images at: imageIndex)