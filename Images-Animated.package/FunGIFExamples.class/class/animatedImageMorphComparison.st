as yet unclassified
animatedImageMorphComparison
	| cat1 cat2 container |
	cat1 := Morph new
		color: Color transparent;
		layoutPolicy: TableLayout new;
		listDirection: #topToBottom;
		listCentering: #center;
		hResizing: #shrinkWrap;
		vResizing: #shrinkWrap.
	cat2 := Morph new
		color: Color transparent;
		layoutPolicy: TableLayout new;
		listDirection: #topToBottom;
		listCentering: #center;
		hResizing: #shrinkWrap;
		vResizing: #shrinkWrap.
	container := Morph new
		color: (Color white alpha: 0.7);
		layoutPolicy: TableLayout new;
		listDirection: #rightToLeft;
		hResizing: #shrinkWrap;
		vResizing: #shrinkWrap.
	cat1 addMorph: (StringMorph new contents: 'Old AnimatedImageMorph (stepTime 16)').
	cat1 addMorph: (self starKitty stepTime: 16; yourself).
	cat2 addMorph: (StringMorph new contents: 'Better AnimatedImageMorph (stepTime 16)').
	cat2 addMorph: (self starKittyBetter stepTime: 16; yourself).
	container
		addMorph: cat1;
		addMorph: cat2;
		openInWindowLabeled: 'Comparison of Animated Morphs'