as yet unclassified
exampleCatmobile
	| image |
	image := self catmobile.
	"This process will make the cat run off from the right side to the left
	side of the World"
	[
		image position: (World extent x + 10)@((World extent y - image extent y) / 2).
		image openInWorld.
		[ (image position x + image extent x) > 0] whileTrue: [
			image position: (image position x - 12)@(image position y).
			(Delay forMilliseconds: 16) wait.
		].
		image delete.
	] forkNamed: #CatMobile