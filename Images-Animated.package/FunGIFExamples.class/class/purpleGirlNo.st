as yet unclassified
purpleGirlNo
	| resp |
	resp := WebClient httpDo: [ :client | client httpGet: self purpleGirlNoUrl ].
	^TransformationMorph new
		asFlexOf: ( AnimatedImageMorph fromStream: resp content asByteArray readStream);
		scale: 0.5