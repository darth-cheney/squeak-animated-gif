as yet unclassified
compile
	"For each AnimatedImageFrame, use its information and
	disposal to compile individual, full forms for each frame (ie composites)"
	| stream currentForm currentCanvas frame |
	stream := frames readStream.
	frame := stream next.
	currentForm := Form extent: self extent depth: frame form depth.
	currentCanvas := FormCanvas on: currentForm.
	
	"Draw the first frame"
	currentCanvas
		image: frame form
		at: frame offset.
	self forms add: currentCanvas form.
	[ stream atEnd ] whileFalse: [
		frame := stream next.
		currentForm := Form extent: self extent depth: frame form depth.
		currentCanvas := FormCanvas on: currentForm.
		(frame disposal == #leaveCurrent)
			ifTrue: [
				currentCanvas
					image: self forms last
					at: 0@0;
					image: frame form
					at: frame offset ]
			ifFalse: [
				currentCanvas
					fillRectangle: ((0@0) extent: self extent)
					color: self backgroundColor;
					image: frame form
					at: frame offset ].
			forms add: currentCanvas form ]