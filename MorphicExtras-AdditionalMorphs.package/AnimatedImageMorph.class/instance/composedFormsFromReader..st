frame composition
composedFormsFromReader: aGIFReader
	"Compose a collection of Forms that are composited
	from the incoming collection of Frames. We do this instead of
	compositing each Form on the fly for performance reasons.
	With this method, we can achieve better framerates for
	Animated GIFs."
	| nextForm compForm |
	nextForm := Form extent: (aGIFReader canvasWidth)@(aGIFReader canvasHeight) depth: 32.
	^ aGIFReader frames collect: [ :frame |
		frame form displayOn: nextForm at: 0@0 rule: Form paint.
		compForm := nextForm copy.
		(frame disposal = #leaveCurrent)
			ifTrue: [
				nextForm := nextForm copy ].
		(frame disposal = #restoreBackground)
			ifTrue: [
				nextForm := Form extent: (aGIFReader canvasWidth)@(aGIFReader canvasHeight) depth: 32 ].
		compForm ]
	