initialization
fromGIFReader: aGIFReader
	delays := aGIFReader delays.
	self
		stepTime: aGIFReader delays first;
		images: (self composedFormsFromReader: aGIFReader);
		yourself