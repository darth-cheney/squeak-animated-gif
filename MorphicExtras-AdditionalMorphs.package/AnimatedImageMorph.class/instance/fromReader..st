private
fromReader: reader
	| extent |
	extent := (reader canvasWidth)@(reader canvasHeight).
	images := reader forms.
	delays := reader delays.
	self isOpaque: true.
	self reset.