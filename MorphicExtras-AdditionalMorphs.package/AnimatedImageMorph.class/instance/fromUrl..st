as yet unclassified
fromUrl: aUrl
	| request |
	request := WebClient httpDo: [ :client | client httpGet: aUrl ].
	(request code = 200) not ifTrue: [ Error signal: 'Bad URL or Request' ].
	((request headerAt: 'Content-Type') = 'image/gif') not
		ifTrue: [ Error signal: 'Url does not respond with a gif, but a ', (request headerAt: 'Content-Type')].
	^ self fromStream: request content asByteArray readStream.