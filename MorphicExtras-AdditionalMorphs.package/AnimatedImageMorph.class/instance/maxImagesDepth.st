as yet unclassified
maxImagesDepth
	"Respond with the highest depth values among all
	the constituent images"
	^ self images inject: 0 into: [ :sum :form |
		(form depth > sum)
			ifTrue: [ form depth ]
			ifFalse: [ sum ] ]