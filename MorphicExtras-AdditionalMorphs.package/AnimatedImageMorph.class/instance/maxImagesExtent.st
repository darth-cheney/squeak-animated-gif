as yet unclassified
maxImagesExtent
	"Respond with the maximum extent bounded by
	all of the constituent image forms"
	| w h |
	w := 0.
	h := 0.
	images do: [ :form |
		(w < ( form extent x + form offset x))
			ifTrue: [ w := form extent x + form offset x ].
		(h < (form extent y + form offset y))
			ifTrue: [ h := form extent y + form offset y ]].
	^ w@h