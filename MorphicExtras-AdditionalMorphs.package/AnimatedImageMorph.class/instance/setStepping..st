stepping and presenter
setStepping: aBoolean
	self wantsSteps ifFalse:[^false].
	aBoolean ifTrue:[self startStepping]
					ifFalse:[self stopStepping].