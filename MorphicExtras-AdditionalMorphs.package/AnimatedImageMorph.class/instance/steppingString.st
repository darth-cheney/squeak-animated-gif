stepping and presenter
steppingString
	^ (self isStepping
		ifTrue: ['<on>']
		ifFalse: ['<off>']), 'stepping' translated