stepping and presenter
toggleStepping
	self wantsSteps
		ifTrue: [
			self isStepping
				ifFalse: [self startStepping]
				ifTrue: [self stopStepping]]